/*
	Copyright (C) 2022  Masoud Naservand

	This file is part of MemAvailable.

	MemAvailable is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MemAvailable is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with prettyfolder.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

// from htop StringUtils.c
#define String_startsWith(s, match) (strncmp((s),(match),strlen(match)) == 0)

int main(int argc, char *argv[]) {

    unsigned long long int MemAvailable = 0;

    char buffer[128];
    FILE *file = fopen("/proc/meminfo", "r");
    if (file == NULL) {
        perror(NULL);
        exit(EXIT_FAILURE);
    }

    while (fgets(buffer, 128, file)) {
        if (buffer[0] == 'M' && String_startsWith(buffer, "MemAvailable")) {
           int ret = sscanf(buffer + strlen("MemAvailable:"), " %32llu kB", &MemAvailable);
           if (ret != 1) {
               fprintf(stderr, "ERROR: Could not find \"MemAvailable\" in file");
               fclose(file);
               exit(EXIT_FAILURE);

           }
           break;
        }
    }

    MemAvailable /= 1024; //to MiB
    printf("%lluMiB", MemAvailable);
    fclose(file);
    exit(EXIT_SUCCESS);
}

